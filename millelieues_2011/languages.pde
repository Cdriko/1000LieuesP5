////cestion multilangue de l'interface e partir d'un fichier xml

import processing.xml.*;
XMLElement xml;
String language="fr";

void languages(){
  xml = new XMLElement(this, "languages.xml");
  ////compte le nombre de labels
  //int numLabels = xml.getChildCount();
}


String label(String zlabel){
  XMLElement[] labels = xml.getChildren();
  for (int i=0; i<labels.length; i++) {

    String lelabel = labels[i].getStringAttribute("id"); 
    if (lelabel.equals(zlabel)==true){
      //println("label :"+zlabel+".........");
      XMLElement celabel = xml.getChild(i);
      XMLElement[] versions = celabel.getChildren();
      
      for (int j=0; j<versions.length; j++) { ///pour charque version de langue
        String langue = versions[j].getStringAttribute("lang"); 
        
        if (langue.equals(language)==true){
          String texte = versions[j].getStringAttribute("text"); 
          return texte;
          // String aide = versions[j].getContent();
          // println("version :"+langue);
          // println("texte :"+texte);
          //println("aide :"+aide);
        }///fin de cette version

      }///fin parcours versions
    }///fin de ce label
  }///fin parcours labels
return "no label in this version";
}///fin label







///////////////////////////////////////////////////
String aide(String zlabel){
  XMLElement[] labels = xml.getChildren();
  for (int i=0; i<labels.length; i++) {

    String lelabel = labels[i].getStringAttribute("id"); 
    if (lelabel.equals(zlabel)==true){
     /// println("aide :"+zlabel+".........");
      XMLElement celabel = xml.getChild(i);
      XMLElement[] versions = celabel.getChildren();
      
      for (int j=0; j<versions.length; j++) { ///pour charque version de langue
        String langue = versions[j].getStringAttribute("lang"); 
        
        if (langue.equals(language)==true){
          
           String aide = versions[j].getContent();
           return aide;
          // println("version :"+langue);
          // println("texte :"+texte);
          //println("aide :"+aide);
        }///fin de cette version

      }///fin parcours versions
    }///fin de ce label
  }///fin parcours labels
return "";
}///fin aide







