import hypermedia.video.*;
import java.awt.*;


OpenCV opencv;
boolean CVOK=false;

int w = 160;
int h = 120;
int threshold = 80;

//from V1
int  seuilMouvement=50;
int nombremouvements=0; 

int numPixels;
float[] baricentre={0,0};
float[] oldbaricentre={0,0};
float[] centre={160,120};
float[] derivee={0,0};

float total_area;





void init_motion(){

   opencv = new OpenCV( this );

   opencv.capture(w,h);
   numPixels=160*120;

}


void  MotionDetect(){

    //background(100);
    
   
    opencv.read();
  

    image( opencv.image(),0,0,320, 240 );	            // RGB image
    
    opencv.absDiff();
    opencv.threshold(threshold);
   

    // working with blobs
    Blob[] blobs = opencv.blobs( 100, w*h/3, 7, true );


opencv.remember();

    
total_area=0;

    
    for( int i=0; i<blobs.length; i++ ) {///pour toutes les zonnes détectées

        Rectangle bounding_rect	= blobs[i].rectangle;
        float area = blobs[i].area;
        
        Point centroid = blobs[i].centroid;
        Point[] points = blobs[i].points;
        
        float barre=area*(blobs[i].centroid.x-(w/2));
        
        baricentre[0] = baricentre[0]+ barre;
        baricentre[1] += area*(blobs[i].centroid.y-(h/2));
        total_area += area;
       
        // rectangle
        noFill();
        stroke( blobs[i].isHole ? 128 : 64 );
        rect(2* bounding_rect.x,2* bounding_rect.y, 2*bounding_rect.width, 2*bounding_rect.height );

        // centroid
        stroke(0,0,255);
        line( centroid.x-5, centroid.y, centroid.x+5, centroid.y );
        line( centroid.x, centroid.y-5, centroid.x, centroid.y+5 );
    }
    
    
    ///bilan de la detection = mise à niveau du baricentre
   
   if (blobs.length>0){
    baricentre[0]=(baricentre[0]/total_area)/blobs.length;
    baricentre[1]=(baricentre[1]/total_area)/blobs.length;    
    
    
   }else{
    baricentre[0]=0;
      baricentre[1]=0;
   }
   
   float[] newderivee={0,0};
    ///mise e jour de la derivee du mouvemlent
    newderivee[0]=(baricentre[0]-oldbaricentre[0])/14;
    newderivee[1]=(baricentre[1]-oldbaricentre[1])/14; 

    oldbaricentre[0]=oldbaricentre[0]+(newderivee[0]);     
    oldbaricentre[1]=oldbaricentre[1]+(newderivee[1]);

    ///memorise la derivee amortie
    derivee[0]= derivee[0]+ (newderivee[0]-derivee[0])/3;
    derivee[1]= derivee[1]+ (newderivee[1]-derivee[1])/3;
    
    
     ///dessin du vecteur de mouvement
     stroke(0, 250, 0);
       noFill();
       ellipseMode(CENTER);
        strokeWeight(1);
        
        println(total_area);
        int dia=int(total_area*500/numPixels);
        if(dia>240){dia=240;}
        ellipse(160,120, dia, dia);
        ellipse(160,120,8, 8);

        line(0,120,20,120);
        line(300,120,320,120);
        line(160,0,160,20);
        line(160,220,160,240);

        line(160, 120, oldbaricentre[0]+160, oldbaricentre[1]+120);
        fill(0,250,0);
        ellipse(oldbaricentre[0]+160, oldbaricentre[1]+120,4, 4);
    
}




public void stop() {
    opencv.stop();
    ///déconnection du serveur
    if(traces.value==1){
             if(envol==1){      
                         String position=longitude+","+latitude+","+altitude;
                         encours=1;
                         requetephp("refresh.php?user=" +MonNom + "&position="+position+"&heading="+heading+"&aterrissage=1"+"&numvol="+NumeroVol);
                         while(encours==1) {    
                                println("attente d'une réponse");
                                text("aterrissage...", 15, 60);
                          }
                          println("au sol");
             }
    } 
    
    //arrêt des moteurs
    super.stop();
}






