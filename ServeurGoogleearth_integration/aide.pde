/*
prototype de connection entre processing et Google earth sous linux


L'api Google earth n'étant pas disponible sous linux




USAGE :

1- lancer le sketch processing
2- dans Google earth ajouter>lien réseau
  url : http://localhost:5488/hud.kml
3- Outils > Navigation : vitesse de l'accès direct , régler à 2
4- la souris dans la fentre du sketch processing pilote l'angle de vue



FONCTIONNEMENT :

Le "lien réseau" fait une requete sur le serveur/processing
la réponse du serveur est, le premier coup, le fichier KML suivant :

---------------------------------------------------------------------
<kml xmlns="http://earth.google.com/kml/2.2">
<Document id="hud"><name>1OOOL</name> 
<ScreenOverlay id="boussoule">
<name>boussole</name>
<visibility>1</visibility>
<Icon><href>http://earth.1000lieues.net/media/Logo.png</href></Icon>
<overlayXY x="0.5" y="0.5" xunits="fraction" yunits="fraction"/>
<screenXY x="0.1" y="0.1" xunits="fraction" yunits="fraction"/>
<rotationXY x="0.5" y="0.5" xunits="fraction" yunits="fraction"/>
<size x="0.2" y="0" xunits="fraction" yunits="fraction"/>
<rotation>0</rotation>
</ScreenOverlay>

<Folder id="updates" >
<name>updates</name>
<Folder id="refresh" >
<name>refresh</name>
<NetworkLink> 
<Camera id="lacamera"><longitude>-1.302595374442352</longitude><latitude>47.1974066095962</latitude><altitude>170</altitude><heading>0</heading><tilt>60</tilt><roll>0</roll><altitudeMode>relativeToGround</altitudeMode>  </Camera>
<flyToView>1</flyToView>
<Link><href>http://localhost:5844/refresh</href><refreshMode>onExpire</refreshMode> </Link></NetworkLink>
</Folder>
</Folder>
</Document> 
</kml>

------------------------------------------------------------------------------------------
"<refreshMode>onExpire</refreshMode>" signifie que le lien de mise a jour s'activera à l'expiration de la page

or, la date d'expiration est très ancienne
donc le rafraichissement est immédiat

la réponse a http://localhost:5844/refresh est :
---------------------------------------------------------------


<kml xmlns="http://earth.google.com/kml/2.2">
<NetworkLinkControl>
<expires>1977-04-22T01:00:00-05:00</expires>
<Update>
<targetHref>http://localhost:5844/hud.kml</targetHref>
<Change>
<ScreenOverlay targetId="boussoule"><name>boussole110.0</name><rotation>85.0</rotation></ScreenOverlay> 
<Camera targetId="lacamera"><longitude>-1.3025954</longitude><latitude>47.197407</latitude><roll>-85.0</roll><tilt>110</tilt><heading>110.0</heading></Camera>
</Change>
</Update>
</NetworkLinkControl>
</kml>

-------
met à jour la position de la camera et la rotation de l'image de la boussole

*/
