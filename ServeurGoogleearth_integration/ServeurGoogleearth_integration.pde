/**serveur de dialogue en http avec GoogleEarth
 * from Chat Server 
 * by Tom Igoe. 
 * 
 */

import processing.net.*;

PFont font;
int port = 5844;
boolean myServerRunning = true;
int bgColor = 0;
int direction = 1;
int start=0;




float roll=0;
Server myServer;

float[] position={
  -1.302595374442352,47.1974066095962,150.};
float[] orientation={
  0.,0.,0.}; //heading,tilt,roll


void setup()
{
  size(180,180);
  println("demarrage du serveur local http sur le port "+port);
  myServer = new Server(this, port); // Starts a myServer on port 10002
  //refreshPosition("0,30,2000");


}

public void stop(){
  myServer.stop();
  myServer.dispose();
  myServerRunning = false;
  super.stop();
}



void draw()
{
  moteur_de_vol();
  //
  if (myServerRunning == true)
  {
    //text("server", 15, 25);
    Client thisClient = myServer.available();
    if (thisClient != null) {
      if (thisClient.available() > 0) {
        println("un message de: " + thisClient.ip());
        String requete=thisClient.readString();
        println(requete);
        ///extraction du nom de la page demandee
        String[] mots = split(requete,"HTTP");
        requete=mots[0];

        mots=split(requete,"GET");
        String page="blank";
        if(mots.length>1){
         page =trim(mots[1]);
        }
        
        mots=split(page,"/");
        page=mots[1];
        
        


        ////construction de la reponse
        String reponse="HTTP/1.1 200 OK\n";
        reponse=reponse+"Connection: close\n";
        reponse=reponse+"Date: Thu, 21 Jun 2007 08:28:57 GMT \n" ;
        reponse=reponse+"Cache-Control: no-cache, must-revalidate\n";
        reponse=reponse+" Expires: Thu, 01 Dec 1994 16:00:00 GMT \n";
        reponse=reponse+"Content-Type: text/html\n\n";


if (page!=null){
        
        println("il veut la page : *"+page+"*");
        reponse=reponse+html(page);
}


        ////println("reponse ===="+reponse);
        thisClient.write(reponse);
        myServer.disconnect(thisClient);

      }
    }
  } 
  else 
  {
    println("serveur arrete");
  }

}




/////pages demandees
String html(String page)
{
  String out="<kml xmlns=\"http://earth.google.com/kml/2.2\">"; 



  if(page.equals("hud.kml") ==true)//création des éléments au départ
  {

    out=out+"<Document id=\"hud\"><name>1OOOL</name>\n";
    out=out+"<ScreenOverlay id=\"boussoule\"><name>boussole</name>";
    out=out+"<visibility>1</visibility><Icon><href>http://earth.1000lieues.net/media/Logo.png</href></Icon>";
    out=out+"<overlayXY x=\"0.5\" y=\"0.5\" xunits=\"fraction\" yunits=\"fraction\"/>";
    out=out+"<screenXY x=\"0.1\" y=\"0.1\" xunits=\"fraction\" yunits=\"fraction\"/>";
    out=out+"<rotationXY x=\"0.5\" y=\"0.5\" xunits=\"fraction\" yunits=\"fraction\"/>";
    out=out+"<size x=\"0.2\" y=\"0\" xunits=\"fraction\" yunits=\"fraction\"/>";
    out=out+"<rotation>0</rotation></ScreenOverlay>";





    ///lien vers l'update
    String NomUpdate="refresh";

    out +="<Folder id=\"updates\" ><name>updates</name><Folder id=\""+NomUpdate+"\" ><name>"+NomUpdate+"</name>";
    out +="<NetworkLink>";




    ///camera
    out +=" <Camera id=\"lacamera\"><longitude>-1.302595374442352</longitude><latitude>47.1974066095962</latitude><altitude>170</altitude>";   
    out +="<heading>0</heading><tilt>60</tilt><roll>0</roll>";
    out +="<altitudeMode>relativeToGround</altitudeMode>  </Camera>";

    

    //  out +=" <gx:flyToMode>bounce</gx:duration> ";
    out +="<flyToView>1</flyToView>";

    out +="<Link>";
    out +="<href>http://localhost:"+port+"/"+NomUpdate+"</href>";
   
    out +="<refreshMode>onExpire</refreshMode> </Link></NetworkLink></Folder></Folder>";

    out=out+"</Document>\n";
  }

  else
  {//////c'est une mise e jour
    roll =mouseX-90;
    float head= mouseY;
    //roll=0;
    // float longitude=0.1-(random(20)/100);
    float longitude=0;
    out += "<NetworkLinkControl>";

    out += "<expires>1977-04-22T01:00:00-05:00</expires><Update>";
    out += "<targetHref>http://localhost:"+port+"/hud.kml</targetHref>";
    out += "<Change><ScreenOverlay targetId=\"boussoule\"><name>boussole"+head+"</name>"; 
    out += "<rotation>"+(-roll)+"</rotation></ScreenOverlay>";
    out +=" <Camera targetId=\"lacamera\"><longitude>"+position[0]+"</longitude><latitude>"+position[1]+"</latitude><roll>"+roll+"</roll><tilt>"+mouseY+"</tilt><heading>"+head+"</heading></Camera></Change></Update>";




    out += "</NetworkLinkControl>"; 


  }

  ///c'est un update


  out=out+"</kml>";
  return out;
}




